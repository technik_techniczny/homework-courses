# homework-courses

This is example solution for: http://docs.coursemgmt.apiary.io/#reference.

## Install

Firstly clone repository:
```bash
git clone git@gitlab.com:t3c-1337/homework-courses.git
```

Enter directory
```bash
cd homework-courses
```

Install dependencies via composer
```bash
composer install
```
if you dont have Composer installed, please refer to: https://getcomposer.org/

Copy default environment configuration
```bash
cp .env.example .env
```
we don't need to do anything with configuartion. For purpuses of this example is all ok.

Framework requires us to setup encryption key after fresh install
```bash
php artisan key:generate
```
For the purpuses of this example we are going to use sqlite database. Create one
```bash
touch database/database.sqlite
```

Create database schema
```bash
php artisan migrate
```

Finally run local server via php extension with
```bash
php artisan serve
```
From now on you should access site with url http://localhost:8000
