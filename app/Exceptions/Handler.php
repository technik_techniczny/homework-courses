<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Contracts\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        $status = 'error';
        $message = null;
        $data = null;
        $code = 400;

        /**
         * Model Not Found
         */
        if ($e instanceof ModelNotFoundException || $e instanceof NotFoundHttpException)
        {
            $message = 'Not Found';
            $code = 404;
        }

        /**
         * Validation Error
         */
        elseif ($e instanceof ValidationException)
        {
            $message = 'Validation Error';
            $data['errors'] = $e->errors()->all();
        }

        /**
         * Other errors
         */
        else {
            $message = $e->getMessage();
            $code = 500;
        }

        return response()->json(array_filter(compact('status', 'message', 'data')), $code);
    }
}
