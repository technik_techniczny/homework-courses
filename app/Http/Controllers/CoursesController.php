<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Course;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return [
            'status' => 'success',
            'data' => [
                'courses' => Course::where([])->with('candidates')->get()->toArray()
            ]
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('title', 'candidate_limit', 'begin', 'end');

        // Validate
        $rules = [
            'title' => 'required',
            'begin' => 'required' ,
            'end' => 'required',
            'candidate_limit' => 'required'
        ];
        $validator = app('validator')->make($data, $rules);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Fields begin, end, candidate_limit and title are required'
            ], 400);
        }

        // Create
        $course = Course::create($data);
        return response()->json([
            'status' => 'success',
            'data' => [
                'id' => $course->id
            ]
        ], 201);
    }

    /**
     * Register a User to a Course
     *
     * @param  Request $request
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request, $id)
    {
        $course = Course::findOrFail($id);

        // verify candidate limit
        if ($course->candidates()->count() >= $course->candidate_limit)
        {
            return response()->json([
                'status' => 'fail',
                'data' => [
                    'message' => 'Candidate Limit reached',
                    'candidate_limit' => $course->candidate_limit
                ]
            ]);
        }

        $user = User::findOrFail($request->get('id_user'));

        // verify not yet enlisted
        if ($course->candidates()->where('user_id', '=', $user->id)->first())
        {
            return response()->json([
                'status' => 'fail',
                'data' => [
                    'message' => 'Candidate has already been enlisted',
                ]
            ]);
        }

        $course->candidates()->attach($user->id);

        return response()->json([
            'status' => 'success',
            'data' => null
        ], 201);
    }

    /**
     * Unregister a user from course
     * @param  Request $request
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function unregister(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $user = $course->candidates()->where('user_id','=', $request->get('id_user'))->first();

        // verify
        if (! $user)
        {
            return response()->json([
                'status' => 'error',
                'message' => "Candidate not found"
            ]);
        }

        // detach
        $course->candidates()->detach($user->id);

        return [
            'status' => "success",
            'data' => null
        ];
    }
}
