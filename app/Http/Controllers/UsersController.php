<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->middleware('validate:App\User', ['only' => ['store']]);
        $this->middleware('validate:App\User,update', ['only' => ['update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return [
            'status' => 'success',
            'data' => [
                'users' => User::all()->toArray()
            ]
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('first_name', 'last_name', 'gender');
        $user = User::create($data);

        return response()->json([
            'status' => 'success',
            'data' => [
                'id' => $user->id
            ]
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $data = array_filter($request->only('first_name', 'last_name', 'gender'));
        $user->update($data);

        return [
            'status' => 'success',
            'data' => null
        ];
    }
}
