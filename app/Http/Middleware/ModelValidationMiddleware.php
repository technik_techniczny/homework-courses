<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Validation\ValidationException;

class ModelValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string $model_class Full class name of model
     * @return mixed
     */
    public function handle($request, Closure $next, $model_class, $rule_set = 'create')
    {
        $model_instance = app($model_class);
        $validator = app('validator')->make($request->input(), $model_instance->rules[$rule_set]);

        if ($validator->fails())
        {
            throw new ValidationException($validator);
        }

        return $next($request);
    }
}
