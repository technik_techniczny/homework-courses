<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Users
 */
Route::resource('users', 'UsersController', ['only' => ['index', 'store', 'update']]);

/**
 * Courses
 */
Route::get('courses', 'CoursesController@index');
Route::post('courses', 'CoursesController@store');
Route::post('courses/{id}/register', 'CoursesController@register');
Route::delete('courses/{id}/register', 'CoursesController@unregister');