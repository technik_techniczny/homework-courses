<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Course extends Model
{
    /**
     * Validation Rules
     * @var array
     */
    public $rules = [
        'create' => [
            'title' => 'required|string',
            'candidate_limit' => 'required|integer',
            'begin' => 'required|date_format:U',
            'end' => 'required|date_format:U'
        ]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'candidate_limit', 'begin', 'end'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    /**
     * Disable timestamps
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['begin', 'end'];

    /**
     * Casting data
     */
    protected $casts = [
        'id' => 'int',
        'end' => 'int',
        'begin' => 'int',
        'candidate_limit' => 'int'
    ];

    /**
     * Date format
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Users relationship
     */
    public function candidates()
    {
        return $this->belongsToMany(User::class, 'courses_users');
    }
}
