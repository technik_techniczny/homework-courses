<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Course;

class User extends Model
{
    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'create' => [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'gender' => 'required|in:m,w'
        ],
        'update' => [
            'first_name' => 'sometimes|string',
            'last_name' => 'sometimes|string',
            'gender' => 'sometimes|in:m,w'
        ]
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'gender'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    /**
     * Disable timestamps
     */
    public $timestamps = false;

    /**
     * Casting data
     */
    protected $casts = [
        'id' => 'int'
    ];

    /**
     * Relationship to Courses
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'courses_users');
    }

}
